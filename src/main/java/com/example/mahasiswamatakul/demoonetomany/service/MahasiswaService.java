package com.example.mahasiswamatakul.demoonetomany.service;


import com.example.mahasiswamatakul.demoonetomany.dao.MahasiswaDao;
import com.example.mahasiswamatakul.demoonetomany.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MahasiswaService {

    @Autowired
    private MahasiswaDao mahasiswaDao;

    public List<Mahasiswa> getMahasiswaAll(){
        return mahasiswaDao.findAll();
    }

    public Optional<Mahasiswa> getMahasiswaById(Long mahasiswaid) {
        if (mahasiswaDao.existsById(mahasiswaid)) {

            return mahasiswaDao.findById(mahasiswaid);
        }
        else{
            throw new ResourceNotFoundException("Mahasiswa dengan " + mahasiswaid + "tidak ditemukan");
        }

    }

    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa){
        return mahasiswaDao.save(mahasiswa);
    }

    public Mahasiswa updateMahsiswaById(Long mahasiswaid, Mahasiswa mahasiswaRequest){
        if (!mahasiswaDao.existsById(mahasiswaid)){
            throw new ResourceNotFoundException("Mahasiswa dengan"  + mahasiswaid + "tidak ditemukan");
        }
        Optional<Mahasiswa> mahasiswa = mahasiswaDao.findById(mahasiswaid);

        if (!mahasiswa.isPresent()){
            throw new ResourceNotFoundException("Mahasiswa dengan"  + mahasiswaid + "tidak ditemukan");
        }

        Mahasiswa mahasiswa1 = mahasiswa.get();
        mahasiswa1.setName(mahasiswaRequest.getName());
        mahasiswa1.setGender(mahasiswaRequest.getGender());
        mahasiswa1.setNik(mahasiswaRequest.getNik());
        return mahasiswaDao.save(mahasiswa1);
    }

    public ResponseEntity<Object> deleteById(Long mahasiswaid){
        if (!mahasiswaDao.existsById(mahasiswaid)){
            throw new ResourceNotFoundException("Mahasiswa dengan"  + mahasiswaid + "tidak ditemukan");
        }

        mahasiswaDao.deleteById(mahasiswaid);
        return ResponseEntity.ok().build();
    }

}
