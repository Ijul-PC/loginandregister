package com.example.mahasiswamatakul.demoonetomany.service;


import com.example.mahasiswamatakul.demoonetomany.dao.UserDao;
import com.example.mahasiswamatakul.demoonetomany.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);

        if (user==null) new UsernameNotFoundException("User Not Found");
        return user;
    }

    @Transactional
    public User loadUserById(Long id){
        User user = userDao.getById(id);
        if (user==null) new UsernameNotFoundException("User Not Found");
        return user;
    }
}
