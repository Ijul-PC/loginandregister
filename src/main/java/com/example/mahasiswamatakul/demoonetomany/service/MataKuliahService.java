package com.example.mahasiswamatakul.demoonetomany.service;

import com.example.mahasiswamatakul.demoonetomany.dao.MataKuliahDao;
import com.example.mahasiswamatakul.demoonetomany.entity.Mahasiswa;
import com.example.mahasiswamatakul.demoonetomany.entity.MataKuliah;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MataKuliahService {


    @Autowired
    private MataKuliahDao mataKuliahDao;

    public List<MataKuliah> getMatakuliahAll(){
        return mataKuliahDao.findAll();
    }

    public Optional<MataKuliah> getMatakuliahById(Long matakuliahid) {
        if (mataKuliahDao.existsById(matakuliahid)) {

            return mataKuliahDao.findById(matakuliahid);
        }
        else{
            throw new ResourceNotFoundException("Mahasiswa dengan " + matakuliahid + "tidak ditemukan");
        }

    }

    public MataKuliah createMatakuliah(MataKuliah mataKuliah){
        return mataKuliahDao.save(mataKuliah);
    }

    public MataKuliah updateMatakuliahById(Long matakuliahid, MataKuliah matakuliahRequest){
        if (!mataKuliahDao.existsById(matakuliahid)){
            throw new ResourceNotFoundException("Mahasiswa dengan"  + matakuliahid + "tidak ditemukan");
        }
        Optional<MataKuliah> matakuliah = mataKuliahDao.findById(matakuliahid);

        if (!matakuliah.isPresent()){
            throw new ResourceNotFoundException("Mahasiswa dengan"  + matakuliahid + "tidak ditemukan");
        }

        MataKuliah matakuliah1 = matakuliah.get();
        matakuliah1.setSubject(matakuliahRequest.getSubject());
        matakuliah1.setJam(matakuliahRequest.getJam());
        matakuliah1.setKelas(matakuliahRequest.getKelas());
        matakuliah1.setAbsent(matakuliahRequest.getAbsent());
        return mataKuliahDao.save(matakuliah1);
    }

    public ResponseEntity<Object> deleteById(Long matakuliahid){
        if (!mataKuliahDao.existsById(matakuliahid)){
            throw new ResourceNotFoundException("Mahasiswa dengan"  + matakuliahid + "tidak ditemukan");
        }

        mataKuliahDao.deleteById(matakuliahid);
        return ResponseEntity.ok().build();
    }
}
