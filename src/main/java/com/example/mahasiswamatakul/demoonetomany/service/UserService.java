package com.example.mahasiswamatakul.demoonetomany.service;

import com.example.mahasiswamatakul.demoonetomany.dao.UserDao;
import com.example.mahasiswamatakul.demoonetomany.entity.User;
import com.example.mahasiswamatakul.demoonetomany.exception.UsernameAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User saveUser (User newUser) {
        try {
            newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));

            //newUser.setUsername(newUser.getUsername()); // bisa dipake bisa ngga

            newUser.setConfirmPassword("");

            return userDao.save(newUser);

        } catch (Exception e) {
            throw new UsernameAlreadyExistException("Username " + newUser.getUsername() + "' already exist");
        }
    }


}
