package com.example.mahasiswamatakul.demoonetomany.exception;

public class UsernameAlreadyExist {

    private String username;

    public UsernameAlreadyExist(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
