package com.example.mahasiswamatakul.demoonetomany.entity;


import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table (name = "MataKuliah")
public class MataKuliah implements Serializable{

    @Column(name = "id", nullable = false, length = 10)
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Subject")
    @NotBlank(message = "please input subject")
    private String subject;


    private String jam;
    private String kelas;
    private String absent;

   @ManyToOne //(fetch = FetchType.LAZY)
//    @JoinColumn(name = "mahasiswaid", nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
    private Mahasiswa mahasiswa;

    public MataKuliah() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getAbsent() {
        return absent;
    }

    public void setAbsent(String absent) {
        this.absent = absent;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
}
