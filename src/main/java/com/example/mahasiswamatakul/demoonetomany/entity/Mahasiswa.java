package com.example.mahasiswamatakul.demoonetomany.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Mahasiswa")
public class Mahasiswa implements Serializable {

    @Column(name = "id", nullable = false, length = 10)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String name;
    private String gender;
    private String nik;

    @OneToMany (mappedBy = "mahasiswa", fetch = FetchType.LAZY)
    private Set<MataKuliah> mataKuliahs = new HashSet<>();

    public Mahasiswa() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public Set<MataKuliah> getMataKuliahs() {
        return mataKuliahs;
    }

    public void setMataKuliahs(Set<MataKuliah> mataKuliahs) {
        this.mataKuliahs = mataKuliahs;
    }
}


