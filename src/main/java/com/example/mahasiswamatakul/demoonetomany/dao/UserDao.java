package com.example.mahasiswamatakul.demoonetomany.dao;

import com.example.mahasiswamatakul.demoonetomany.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, Long> {

    User findByUsername(String username);
    User getById(Long id);
}
