package com.example.mahasiswamatakul.demoonetomany.dao;

import com.example.mahasiswamatakul.demoonetomany.entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaDao extends JpaRepository<Mahasiswa, Long> {
}
