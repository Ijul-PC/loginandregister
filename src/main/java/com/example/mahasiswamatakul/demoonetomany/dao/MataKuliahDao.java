package com.example.mahasiswamatakul.demoonetomany.dao;


import com.example.mahasiswamatakul.demoonetomany.entity.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
public interface MataKuliahDao extends JpaRepository<MataKuliah, Long>{

}
