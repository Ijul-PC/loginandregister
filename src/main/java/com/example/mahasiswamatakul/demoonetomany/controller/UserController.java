package com.example.mahasiswamatakul.demoonetomany.controller;

import com.example.mahasiswamatakul.demoonetomany.entity.User;
import com.example.mahasiswamatakul.demoonetomany.login.LoginRequest;
import com.example.mahasiswamatakul.demoonetomany.login.LoginResponse;
import com.example.mahasiswamatakul.demoonetomany.security.JwtTokenProvider;
import com.example.mahasiswamatakul.demoonetomany.service.MapValidationService;
import com.example.mahasiswamatakul.demoonetomany.service.UserService;
import com.example.mahasiswamatakul.demoonetomany.validator.UserValidator;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.example.mahasiswamatakul.demoonetomany.security.SecurityConstant.TOKEN_PREFIX;

@RestController
@RequestMapping(path = "/api/users")
public class UserController {

    @Autowired
    private MapValidationService mapValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserValidator userValidator;

    @PostMapping(path = "/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody User user, BindingResult result){

        userValidator.validate(user, result);

        ResponseEntity<?> errorMap = mapValidationService.MapValidationService(result);

        if (errorMap!= null)
            return errorMap;

        User newUser = userService.saveUser(user);

        return new ResponseEntity<User>(newUser, HttpStatus.CREATED);
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult result){
        ResponseEntity<?> errorMap = mapValidationService.MapValidationService(result);

        if (errorMap!= null)
            return errorMap;

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = TOKEN_PREFIX + jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new LoginResponse(true, jwt));
    }
}
