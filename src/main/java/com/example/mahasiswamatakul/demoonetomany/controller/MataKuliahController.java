package com.example.mahasiswamatakul.demoonetomany.controller;


import com.example.mahasiswamatakul.demoonetomany.dao.MataKuliahDao;
import com.example.mahasiswamatakul.demoonetomany.entity.Mahasiswa;
import com.example.mahasiswamatakul.demoonetomany.entity.MataKuliah;
import com.example.mahasiswamatakul.demoonetomany.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MataKuliahController {


    @Autowired
    private MataKuliahService mataKuliahService;

    @RequestMapping(path = "/matakuliah", method = RequestMethod.GET)
    public List<MataKuliah> getmatakuliahAll(){
        return mataKuliahService.getMatakuliahAll();
    }

    @RequestMapping(path = "/matakuliah/{matakuliahid}", method = RequestMethod.GET)
    public Optional<MataKuliah> getmatakuliahById(@PathVariable(value = "matakuliahid") Long matakuliahid){
        return mataKuliahService.getMatakuliahById(matakuliahid);
    }

    @RequestMapping(path = "/matakuliah", method = RequestMethod.POST)
    public MataKuliah creatematakuliah(@RequestBody MataKuliah matakuliah){
        return mataKuliahService.createMatakuliah(matakuliah);
    }

    @RequestMapping(path = "/matakuliah/{matakuliahid}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public MataKuliah updatematakuliah(@PathVariable(value = "matakuliahid") Long matakuliahid, @RequestBody MataKuliah matakuliah){
        return mataKuliahService.updateMatakuliahById(matakuliahid, matakuliah);
    }

    @RequestMapping(path = "/matakuliah/{matakuliahid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletematakuliahById(@PathVariable(value = "matakuliahid") Long matakuliahid){
        return mataKuliahService.deleteById(matakuliahid);
    }

}
