package com.example.mahasiswamatakul.demoonetomany.controller;

import com.example.mahasiswamatakul.demoonetomany.entity.Mahasiswa;
import com.example.mahasiswamatakul.demoonetomany.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@RestController
public class MahasiwaController {

    @Autowired
    private MahasiswaService mahasiswaService;

    @RequestMapping(path = "/mahasiswa", method = RequestMethod.GET)
    public List<Mahasiswa> getMahasiswaAll(){
        return mahasiswaService.getMahasiswaAll();
    }

    @RequestMapping(path = "/mahasiswa/{mahasiswaid}", method = RequestMethod.GET)
    public Optional<Mahasiswa> getMahasiswaById(@PathVariable(value = "mahasiswaid") Long mahasiswaid){
        return mahasiswaService.getMahasiswaById(mahasiswaid);
    }

    @RequestMapping(path = "/mahasiswa", method = RequestMethod.POST)
    public Mahasiswa createMahasiswa(@RequestBody Mahasiswa mahasiswa){
        return mahasiswaService.createMahasiswa(mahasiswa);
    }

    @RequestMapping(path = "/mahasiswa/{mahasiswaid}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mahasiswa updateMahasiswa(@PathVariable(value = "mahasiswaid") Long mahasiswaid, @RequestBody Mahasiswa mahasiswa){
        return mahasiswaService.updateMahsiswaById(mahasiswaid, mahasiswa);
    }

    @RequestMapping(path = "/mahasiswa/{mahasiswaid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteMahasiswaById(@PathVariable(value = "mahasiswaid") Long mahasiswaid){
        return mahasiswaService.deleteById(mahasiswaid);
    }

}
